class Announcement extends ui.hall.view_announcementUI{
    constructor() {
        super();
        console.log("创建界面");
        this.addEvent();
        Global.opcode.register_event(Opcode.ANNOUNCETION,this,this.setAnnouncement);
        Global.session.Send(Opcode.ANNOUNCETION,null);
    }

    /**设置公告 */
    setAnnouncement(data:any){
        this.label_announcement.text = data.notice;//公告内容
    }

    /**加载事件 */
    addEvent(){
        this.on(Laya.Event.CLICK,this,this.close);
    }
    /**关闭界面 */
    close(){
        this.removeSelf();
    }
}