/*
* 登录;
*/
class LoginUI extends ui.login.loginUI {
    constructor() {
        super();
        // var toggle:Toggle = new Toggle();
        // toggle.x = 100;
        // toggle.y = 200;
        // this.addChild(toggle);


        this.initFont();
        this.initView();
        this.AddEvent();
    }

    /**加载字体 */
    initFont() {
        var text: Laya.Label = new Laya.Label();
        text.font = "button_font";
        text.font = "text_font";
        text.text = "";
        Laya.stage.addChild(text);
        Laya.timer.once(100, this, this.initView);
    }

    private initView() {
        this.label_we_chat_login.font = "button_font";
        this.label_phone_login.font = "button_font";
        this.label_qq_login.font = "button_font";
    }

    private AddEvent() {
        this.login.on(Laya.Event.CLICK, this, this.LoginClick);
        this.fast_login.on(Laya.Event.CLICK, this, this.FastLoginClick);
        this.register.on(Laya.Event.CLICK, this, this.RegisterClick);
        this.view_we_chat_login.on(Laya.Event.CLICK, this, this.weChatLogin);
        this.view_qq_login.on(Laya.Event.CLICK, this, this.qqLogin);
        this.view_phone_login.on(Laya.Event.CLICK, this, this.phoneLogin);
    }
    /**qq登陆 */
    private qqLogin() {
        console.log("qq登陆")
    }

    /***微信登陆 */
    private weChatLogin() {
        console.log("微信登陆");
    }

    /**手机登陆 */
    private phoneLogin() {
        // console.log("手机登陆");
        var loginDialog: LoginDialog = new LoginDialog();
        loginDialog.x = ScreenUtils.centerX(this, loginDialog);
        loginDialog.y = ScreenUtils.centerY(this, loginDialog);
        loginDialog.popupCenter = true;
        Laya.stage.addChild(loginDialog);
    }

    private LoginClick() {
        // Laya.stage.addChild(new LobbyUi());
        Global.session.connect("120.25.94.188", 50001);
        // Global.session.connect("192.168.31.188", 50001)
        Global.session.register_connect_event(this, this.OnLoginConnect);
        Global.session.register_connect_error_event(this, this.OnLoginConnectError);
    }

    private OnLoginConnect() {
        Global.session.unregister_connect_event(this, this.OnFastLoginConnect);
        Global.session.unregister_connect_error_event(this, this.OnLoginConnectError);
        Laya.timer.loop(15000, this, this.Ping);

        var account = this.account.text;
        var password = new MD5().hex_md5(this.password.text);

        var packet = new Object();
        packet['account'] = account;
        packet['password'] = password;

        Global.opcode.register_event(Opcode.LOGIN, this, this.LoginCallBack);
        Global.session.Send(Opcode.LOGIN, packet);
        localStorage.setItem("account", account);
        localStorage.setItem("password", password);
    }

    private FastLoginClick() {
        Global.session.connect("120.25.94.188", 50001);
        // Global.session.connect("192.168.31.188", 50001);
        Global.session.register_connect_event(this, this.OnFastLoginConnect);
        Global.session.register_connect_error_event(this, this.OnLoginConnectError);
    }

    private FastLoginCallBack(data: any) {

    }

    private LoginCallBack(data: any) {
        if (data.result == 1) {
            console.log("登陆失败");
            return;
        }
        Laya.stage.addChild(new LobbyUi());
    }

    private OnLoginConnectError() {
        Global.session.connect("120.25.94.188", 50001);
        // Global.session.connect("192.168.31.188", 50001)
    }

    private OnFastLoginConnect() {
        Global.session.unregister_connect_event(this, this.OnFastLoginConnect);
        Global.session.unregister_connect_error_event(this, this.OnLoginConnectError);
        Laya.timer.loop(15000, this, this.Ping);

        var account = localStorage.getItem("account");
        var password = localStorage.getItem("password");

        if (account == null || password == null) {
            Global.session.Send(Opcode.FAST_LOGIN, packet);
            Global.opcode.register_event(Opcode.FAST_LOGIN, this, this.FastLoginCallBack);
        } else {
            var packet = new Object();
            packet['account'] = account;
            packet['password'] = password;

            Global.session.Send(Opcode.LOGIN, packet);
            Global.opcode.register_event(Opcode.LOGIN, this, this.LoginCallBack);
        }

    }

    private RegisterClick() {

    }

    private Ping() {
        Global.session.Send(Opcode.PING);
    }
}