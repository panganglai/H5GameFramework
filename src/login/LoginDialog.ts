/** 手机登陆 */
class LoginDialog extends ui.login.phone_dialog.phone_loginUI {
    constructor() {
        super();
        this.registerNet();
        this.addEvent();
    }

    registerNet() {
        //Global.opcode.register_event(Opcode.LOGIN,this,this.loginSuccess);
    }
    /**登陆成功 */
    loginSuccess(data: any) {
        
    }

    /**添加事件 */
    addEvent() {
        this.img_close.on(Laya.Event.CLICK, this, this.closeDialog);
        this.view_forget_password.on(Laya.Event.CLICK, this, this.viewForgetPassword);
        this.view_register.on(Laya.Event.CLICK, this, this.register);
        this.view_login.on(Laya.Event.CLICK, this, this.login);
    }
    /**登陆 */
    login() {
        // console.log("登陆");
        Global.session.connect("120.25.94.188", 50001);
        // Global.session.connect("192.168.31.188", 50001)
        Global.session.register_connect_event(this, this.OnLoginConnect);
        Global.session.register_connect_error_event(this, this.OnLoginConnectError);
    }
    /**登陆链接 */
    OnLoginConnect() {
        Laya.timer.loop(15000, this, this.Ping);

        var account = this.input_phone_number.text;
        var password = new MD5().hex_md5(this.input_password.text);

        var packet = new Object();
        packet['account'] = account;
        packet['password'] = password;

        localStorage.setItem("account", account);
        localStorage.setItem("password", password);

        var account = localStorage.getItem("account");
        var password = localStorage.getItem("password");

        if (account == null || password == null) {
            //提示出错
        } else {
            var packet = new Object();
            packet['account'] = account;
            packet['password'] = password;

            Global.session.Send(Opcode.LOGIN, packet);
            Global.opcode.register_event(Opcode.LOGIN, this, this.LoginCallBack);
        }
    }

    private LoginCallBack(data: any) {
        if (data.result == 1) {
            console.log("登陆失败");
            return;
        }
        Laya.stage.addChild(new LobbyUi());
        Laya.stage.addChild(new Announcement());
    }

    /**链接错误 */
    private OnLoginConnectError() {
        Global.session.connect("120.25.94.188", 50001);
        // Global.session.connect("192.168.31.188", 50001)
    }

    /**注册 */
    register() {
        // console.log("注册");
        var registerDialog: RegisterDialog = new RegisterDialog();
        registerDialog.x = this.x;
        registerDialog.y = this.y;
        Laya.stage.addChild(registerDialog);
        this.close();
    }
    /** 忘记密码 */
    viewForgetPassword() {
        console.log("忘记密码");
    }
    /** 关闭窗口 */
    closeDialog() {
        this.close();
    }

    private Ping() {
        Global.session.Send(Opcode.PING);
    }
}