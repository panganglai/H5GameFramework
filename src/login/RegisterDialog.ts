class RegisterDialog extends ui.login.phone_dialog.PhoneRegisterUI {
    constructor() {
        super();
        this.addEvent();
    }

    public addEvent() {
        this.img_close.on(Laya.Event.CLICK, this, this.closeDialog);
    }

    /**关闭窗口 */
    closeDialog() {
        //console.log("关闭对话框");
        var loginDialog: LoginDialog = new LoginDialog();
        loginDialog.x = this.x;
        loginDialog.y = this.y;
        Laya.stage.addChild(loginDialog);
        this.close();
    }

}