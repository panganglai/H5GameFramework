

// 程序入口
class GameMain{
    private assets = [];
    constructor()
    {
        Laya.init(1280,720);
        Laya.Font.defaultFont = "text_font";
        this.assets.push({ url: "res/atlas/comp.json", type: Laya.Loader.ATLAS});
        this.assets.push({ url: "res/atlas/hall.json", type: Laya.Loader.ATLAS});
        this.assets.push({ url: "res/atlas/login.json", type: Laya.Loader.ATLAS});
        this.assets.push({ url: "res/atlas/room.json", type: Laya.Loader.ATLAS});
        this.assets.push({ url: "res/atlas/room/card.json", type: Laya.Loader.ATLAS});
        Laya.loader.load(this.assets, Laya.Handler.create(this, this.onAssetsLoaded));
        //Laya.loader.load([{ url: "res/atlas/comp.json", type: Laya.Loader.ATLAS }], Laya.Handler.create(this, this.onLoaded));
    }
   

    private onAssetsLoaded(): void {
            Laya.stage.addChild(new LoginUI());
			// 使用资源
	}
}

new GameMain();