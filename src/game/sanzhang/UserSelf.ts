/**
* 游戏内玩家自己的数据对象
*/
module game.sanzhang {
	export class UserSelf extends Player {
		cmd: number;
		gender: number;
		last_game_id: string;
		roomno: number;//房间号
		constructor() {
			super();
			this.roomno = 0;
		}

		/**
		 * 从其他类型对象(User,或者Player)拷贝数据到本对象
		 */
		public setUserData(user: User): void {
			this.id = user.id;
			this.name = user.name;
			this.gender = user.sex;
			this.avatar = user.avater;
			this.last_game_id = user.last_game_id;
			this.cmd = user.cmd;
		}

		public setPlayerData(player: game.sanzhang.Player): void {
			this.coin = player.coin;
		}
	}
}