/**
* 游戏内的玩家类
*/
module game.sanzhang{
	export class Player{
		id:number;
		name:string;
		avatar:string;
		coin:number;//当前金币
		constructor(){
			this.id = 0;
			this.name = "";
			this.coin = 0;
		}
	}
}