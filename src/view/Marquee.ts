
/**跑马灯 */
class Marquee extends ui.customView.marqueeUI {
    /**跑马灯文本 */
    public text: string;
    /** 跑马灯距离（每隔事件跳动的距离） */
    public distance: number = 0.02;
    /** 跑马灯时间 */
    public time: number = 1;
    /** 跑马灯的宽度 */
    panelWidth: number;
    /** 公告的长度 */
    announcementWidth: number;
    constructor() {
        super();
    }

    public loadNetText(cmd: number) {
        Global.opcode.register_event(cmd, this, this.start);
        Global.session.Send(cmd, null);
    }

    /**开始 */
    public start(data?) {
        if (data != null) {
            this.text = data.notice;
        }
        this.label_announcement.text = this.text;
        var timer: Laya.Timer = new Laya.Timer();
        timer.clearAll(this);
        //记录label的宽度
        this.panelWidth = this.panel_announcement.width;
        /** 记录文字的宽度 */
        this.announcementWidth = this.label_announcement.width;
        timer.loop(this.time, this, this.startAnnouncement, null, true, true);
    }

    startAnnouncement() {
        //this.panel_announcement.scrollTo(this.i, 0);
        /** 滚动文字的末尾 */
        var scrollWidth: number = this.label_announcement.x + this.announcementWidth;
        //console.log("文字末尾到的地方"+scrollWidth+"");
        /** 滚动一周完成 */
        if (scrollWidth <= -1) {
            this.label_announcement.x = this.panelWidth;
            // this.announcement.x -= 0.02;
        } else {
            this.label_announcement.x -= this.distance;
        }
    }



}