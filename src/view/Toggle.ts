/**开关按钮 */
class Toggle extends ui.customView.toggleButtonUI {
    isOn: boolean = true;//默认是开
    constructor() {
        super();
        this.addEvent();
    }
    /**添加点击事件 */
    addEvent() {
        this.on(Laya.Event.CLICK, this, this.toggleState);
    }
    /**设置状态 */
    setToggle(state: boolean) {
        this.isOn = state;
        if (state) {
            //开
            this.img_first.visible = true;
            this.view_first.x = 0;
        } else {
            //关
            this.img_first.visible = false;
            this.view_first.x = - this.view_first.width + 46;
        }
    }
    /**更换状态 */
    toggleState() {
        this.isOn = !this.isOn;
        this.setToggle(this.isOn);
    }
    /**获取状态 */
    getToggleState(): boolean {
        return this.isOn;
    }
}